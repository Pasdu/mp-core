extends Control

# This script hangs around under the root of the scene and allows debugging
# operations to be performed on the engine.
var output

# Called when the node enters the scene tree for the first time.
func debug_message(message, title = ""):
	output.bbcode_text += "[b]" + str(title) + "[/]: " + str(message) + "\n"

func init():
	visible = false
	var altd = InputEventKey.new()
	
	altd.alt = true
	altd.scancode = KEY_D
	
	InputMap.add_action("debugger")
	
	InputMap.action_add_event("debugger", altd)
	
	output = get_node("contents/scroll/text")
	debug_message("Debug System Initiated","System")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
	if event.is_action_pressed("debugger") and !event.is_echo():
		visible = !visible
