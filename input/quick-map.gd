extends PanelContainer

onready var dialog = get_node("dialog")

onready var player_number = get_node("contents/row1/Player/SpinBox")
onready var dpad_number = get_node("contents/row1/DPads/SpinBox")
onready var analogs_number = get_node("contents/row2/Analogs/SpinBox")
onready var buttons_number = get_node("contents/row2/Buttons/SpinBox")
onready var triggers_number = get_node("contents/row3/Triggers/SpinBox")

onready var start = get_node("contents/row0/start")
onready var select = get_node("contents/row0/select")

var input_settings = preload("res://arcade/loader/input_settings.gd").new()

var input_process = {}

func start_mapping():
	input_process.steps = []
	input_process.events = []
	
	var player = player_number.value
	
	if(start.pressed):
		input_process.steps.push_back("player_" + str(player) + "_start")
	if(select.pressed):
		input_process.steps.push_back("player_" + str(player) + "_select")
	
	for dpad in range(dpad_number.value):
		input_process.steps.push_back("player_" + str(player) + "_dpad_" + str(dpad) + "_up")
		input_process.steps.push_back("player_" + str(player) + "_dpad_" + str(dpad) + "_down")
		input_process.steps.push_back("player_" + str(player) + "_dpad_" + str(dpad) + "_left")
		input_process.steps.push_back("player_" + str(player) + "_dpad_" + str(dpad) + "_right")
	for analog in range(analogs_number.value):
		input_process.steps.push_back("player_" + str(player) + "_analog_" + str(analog) + "_up")
		input_process.steps.push_back("player_" + str(player) + "_analog_" + str(analog) + "_left")
		input_process.steps.push_back("player_" + str(player) + "_analog_" + str(analog) + "_right")
		input_process.steps.push_back("player_" + str(player) + "_analog_" + str(analog) + "_down")
	for button in range(buttons_number.value):
		input_process.steps.push_back("player_" + str(player) + "_button_" + str(button))
	for trigger in range(triggers_number.value):
		input_process.steps.push_back("player_" + str(player) + "_trigger_" + str(trigger))
	
	input_process.current_step = 0
	update_dialog()
	dialog.visible = true

func complete_mapping():
	dialog.visible = false
	input_settings.save_inputs(input_process)

func _input(event):
	if dialog.visible:
		if event.is_action_type() and not event.is_echo() and event.is_pressed():
			var id = input_process.events.size()
			input_process.events.push_back(event)
			input_settings.register_action(input_process.steps[id], event)
			input_process.current_step = input_process.current_step + 1
			update_dialog()

func update_dialog():
	if(input_process.current_step >= input_process.steps.size()):
		complete_mapping()
		return
	dialog.get_node("contents/instruction").text = "Please press the button for " + input_process.steps[input_process.current_step]


func _on_map_pressed():
	start_mapping()


func _on_cancel_pressed():
	visible = false
