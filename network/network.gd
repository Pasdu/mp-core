extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal connection_success

signal add_client(id)
signal output(level, text)


var players_by_id = {}
var players_connected = []
var players_by_nickname = {}
var admins_connected = {}

func join_server(ip, port):
	var peer = NetworkedMultiplayerENet.new()
	db.dbug("Joining server: " + ip + " " + port, "Network")
	peer.connect("connection_failed", self, "_client_connect_fail")
	peer.connect("connection_succeeded", self, "_client_connect_success")
	
	peer.create_client(ip, port)
	get_tree().set_network_peer(peer)

func create_server(SERVER_PORT):

	var peer = NetworkedMultiplayerENet.new()
	var _error = get_tree().connect("network_peer_connected", self, "_other_player_connected")
	_error = get_tree().connect("network_peer_disconnected", self, "_other_player_disconnected")
	var create_server_error = peer.create_server(SERVER_PORT, 32)
	get_tree().network_peer = peer
	
	db.dbug("Creating server on port " + str(SERVER_PORT), "Network")
	
	#db.my_player.id = 1
	return create_server_error

func _client_connect_fail(data):
	print(data)
	db.dbug("Connect Failed" + data, "Network")

# Client Challenge
# Called Remotely by server on clients after they connect succesfully
remote func client_handshake(id, nid, sinid):
	var tests = {
		"id":FAILED,
		"nid":FAILED,
		"sinid":FAILED,
	}
	
	var real_id = get_tree().get_rpc_sender_id()
	if id.length() == 38:
		tests.id = OK
	if nid == real_id:
		tests.nid = OK
	if sin(nid) == sinid:
		tests.sinid = OK
	
	if tests.id == FAILED or tests.nid == FAILED or tests.sinid == FAILED:
		emit_signal("output", "Client " + str(id) + " failed handshake.")
	else:
		register_client(id, nid)
	
	print("Handshake: " + str(tests))
	rpc_id(nid, "profile_request")

# Called by server to register clients
func register_client(id, nid):
	var player_data = { "instance":null }
	
	players_connected.push_back(player_data)
	players_by_id[nid] = player_data
	players_by_nickname[id] = player_data

# Called by a client after it joins a server succesfully
func _client_connect_success():
	var id = str(OS.get_unique_id())
	var nid = get_tree().get_network_unique_id()
	
	rpc_id(1, "client_handshake", id, nid, sin(nid))
	
	## THIS IS THE OLD HANDSHAKE
	#db.my_player.player = get_tree().get_network_unique_id()
	#rpc_id(1,"request_map", character)
	db.dbug("Connected succesfully!", "Network")
	emit_signal("connection_success")

# Called by server when client connects
func _other_player_connected(id):
	emit_signal("add_client", id)
	emit_signal("output", "global", "Player by ID [b]" + String(id) + "[/b] connected.")
	db.dbug("Player by ID [b]" + String(id) + "[/b] connected.", "Network")
	
# Called by server when client disconnects
func _other_player_disconnected(id):
	var player = players_by_id.get(id)
	if player != null:
		emit_signal("remove_client", id)
	
		remove_player(id)
		
		emit_signal("output", "global", "Player by ID [b]" + String(id) + "[/b] disconnected.")
		db.dbug("Player by ID [b]" + String(id) + "[/b] disconnected.", "Network")

func remove_player(id):	
	var player = players_by_id[id]
	if players_by_nickname.get(player.data.nickname):
		admins_connected.erase(player)
		var map = player.map
		if map != null:
			map.remove_player(player)
		emit_signal("remove_player", player.data.nickname)
		players_by_nickname.erase(player.data.nickname)
	players_by_id[id].queue_free()
	players_by_id.erase(id)

## Called from server to client to prompt profile data
remote func profile_request():
	rpc_id(1, "profile_response", db.get_network_profile())
	
remote func profile_response(data):
	var id = get_tree().get_rpc_sender_id()
	register_profile_data(id, data)
	
## Add a user profile to their dataset
func register_profile_data(id, data):
	var player = players_by_id.get(id)
	
	if player != null:
		player.data = data

