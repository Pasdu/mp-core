extends KinematicBody

var totalDelta = 0
var is_master = false

func push_orientation():
	rpc_unreliable("update_orientation", rotation)

func push_position(velocity):
	rpc_unreliable("update_position", translation, velocity)

puppet func update_position(new_position, new_velocity):
	translation = new_position
	self.character_velocity = new_velocity
# Called when the node enters the scene tree for the first time.

func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(is_master):
		if(totalDelta > 0.1):
			push_position(Vector3(0,0,0))
			push_orientation()
			emit_signal("data_push")
			totalDelta=0
		
		totalDelta += delta		
