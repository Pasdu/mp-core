extends Node

var BlockMap
var players = {"data":{}}

signal ready_to_play

var map_data
var height_map = {}

var me

var map_seed

var chunks = {}

var containers

func init( ):
	## Create chunks for the map
	containers = {
		"players":get_node("players"),
		"enemies":get_node("zombies"),
		"items":get_node("items"),
		"chunks":get_node("chunks"),
		"projectiles":get_node("projectiles")
	}

func initialize_map_data(init_data):
	
	self.map_data = init_data
	set_name(map_data.header.name)
	
	map_seed = map_data.header.get("seed", randi())
	
	CHUNKS_X = ceil(map_data.header.resolution.x/CHUNK_SIZE)
	CHUNKS_Y = ceil(map_data.header.resolution.y/CHUNK_SIZE)
	#db.projectiles = containers.projectiles


	for tile in map_data.tiles:
		var tile_data = map_data.tiles[tile]
		var chunk_num = get_chunk_number(tile)
		
		var chunk = chunks.get(chunk_num)
		tile_data.chunk = chunk_num
		height_map[tile] = tile_data.height
		
		if(chunk == null):
			var chunk_ref = db.get_world_object({"store":"world", "query":"chunk"})
			chunk = chunk_ref.object.instance()
			chunk.name = str(chunk_num)
			containers.chunks.add_child(chunk)
			chunk.initialize(self)
			chunks[chunk_num] = chunk
		
		chunk.add_tile(tile, tile_data)
	
	for chunk in containers.chunks.get_children():
		chunk.generate_tile_data()
		chunk.draw()
	load_complete()

func load_complete():
	emit_signal("ready_to_play")
		
remote func join_game(id):
	pass
	
remote func remove_player(id):
	var player = players.get(id)
	if player != null:
		player.queue_free()
		players.erase(id)

func spawn_player(id, coordinates = Vector3(0,0,0)):
	print("hey")
	var player = players[id]
	

	var item	

	if id == 1:
		item = {"store":"mp", "query":"my_player"}
	else:
		item = {"store":"mp", "query":"other_player"}
	
	var item_object = db.get_world_object(item).object.instance()
	item_object.init(players.get(id, {}),self)
	item_object.translation = coordinates
	containers.players.add_child(item_object)
	

remote func despawn_player(id):
	remove_child(players[id])

func spawn_item(item, coordinates):
	var item_object = db.get_world_object(item)
	item_object.translation = coordinates
	containers.items.add_child(item_object)
	
#func spawn_zombie(data):
#	var zombie = preload("res://client/zombie.tscn").instance()
#	zombie.init(data)
#	containers.enemies.add_child(zombie)

func save():
	pass
	
remotesync func change_map(changes):
	var affected_chunks = []
	for coords in changes:
		var change = changes[coords]
		var change_chunk = map_data.tiles[coords].chunk
		if affected_chunks.find(change_chunk) == -1:
			affected_chunks.push_back(change_chunk)
		chunks[change_chunk].update_tile_data(coords, changes[coords])

		var mats = change.get("materials")
		var height = change.get("height")
		var pieces = change.get("pieces")
		
		if mats != null:
			map_data.tiles[coords].materials = mats
		if height != null:
			map_data.tiles[coords].height = height
		if pieces != null:
			map_data.tiles[coords].pieces = pieces
		
	for chunk in affected_chunks:
		
		chunks[chunk].generate_tile_data()
		chunks[chunk].draw_map()

remote func map_ready():
	var id = get_tree().get_rpc_sender_id()
	spawn_player(id)
	rpc_id(id,"set_players", players.data)
	rpc_id(id, "join_game", id)
	
func get_chunk(chunk_number):
	var chunk	
	if(chunk_number < containers.chunks.get_children().size()):
		chunk = containers.chunks.get_child(chunk_number)
	
	if(chunk == null):
		chunk = preload("chunk.tscn").instance()
		containers.chunks.add_child(chunk)
	
	return chunk
	
func draw():
	for chunk in containers.chunks.get_children():
		chunk.draw()
		
var TILE_SIZE = 20
var CHUNK_SIZE = 15
var CHUNKS_Y
var CHUNKS_X

func world_to_grid(world):
	if(world.z != null):
		return Vector2(floor(world.x/TILE_SIZE), floor(world.z/TILE_SIZE))
	else:
		return Vector2(floor(world.x/TILE_SIZE), floor(world.y/TILE_SIZE))

func grid_to_world(grid):
	return(Vector3(grid.x*TILE_SIZE,0, grid.y*TILE_SIZE))

func grid_to_world_center(grid):
	return(Vector3((grid.x*TILE_SIZE) + TILE_SIZE/2,0, (grid.y*TILE_SIZE) + TILE_SIZE/2))

func get_vert_coords(tile_coords):
	var world_coords = grid_to_world(tile_coords)
	
	var result = {
		"BL":world_coords,
		"BR":Vector3(TILE_SIZE,0,0) + world_coords,
		"TL":Vector3(0,0,TILE_SIZE) + world_coords,
		"TR":Vector3(TILE_SIZE,0,TILE_SIZE) + world_coords
	}
	
	return result


	
func get_chunk_number(coords):
	var chunk_x = int(coords.x) % int(CHUNK_SIZE)
	
	var chunk_y = int(coords.y / CHUNK_SIZE)
	
	#var chunks_x = ceil(segment.header.resolution.x/CHUNK_SIZE)
	#var chunks_y = ceil(segment.header.resolution.y/CHUNK_SIZE)		
	
	var chunk = (chunk_y * (CHUNKS_X)) + chunk_x
	return int(chunk)
