extends "res://mp-core/world/map.gd"

func add_player(id, data):
	players[id] = data


remote func set_players(player_data):
	for player in player_data:
		add_player(player, player_data[player])
		spawn_player(player.player)
