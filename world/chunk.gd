extends Node


var floor_collider
var floors

var init = false

var tiles = {}

var draw = false

var entities = {}
var vertex_heights = {}

var map_ref

# PoolVectorXXArrays for mesh construction.
var verts = PoolVector3Array()
var uvs = PoolVector2Array()
var normals = PoolVector3Array()
var indices = PoolIntArray()

func initialize(map_object):
	map_ref = map_object
	floors = get_node("Ground Mesh")
	floor_collider = get_node("Floor Collision/CollisionShape")
	init = true
	
func register_entity(coords, data):
	var entity_model = db.stores.entities.get_entity(data.get("category"), data.get("title")).object.instance()
	
	entity_model.transform.origin = map_ref.grid_to_world_center(coords)
	add_child(entity_model)
	entity_model.init(self)
	var offset = data.get("offset", {})
	
	entity_model.translation.x += offset.get("x",0)
	entity_model.translation.y += offset.get("y",0)
	entity_model.translation.z += offset.get("z",0)
	entities[coords].push_back(entity_model)

func add_tile(coords, tile_data):
	coords = Vector2(floor(coords.x), floor(coords.y))
	
	tiles[coords] = tile_data
	entities[coords] = []

func generate_tile_data():
	for coords in tiles:
		var data = tiles[coords]
		var tile_verts = map_ref.get_vert_coords(coords)	
		var tile_heights = calculate_y(coords)
			
		
		var tile_data = {"corners":{"TL":-1, "TR":-1, "BL":-1, "BR":-1} }
		
		var tile_normals = {}
		
		vertex_heights[coords] = {}
		tile_verts.TL = tile_verts.TL + Vector3(0,tile_heights.TL,0)
		vertex_heights[coords].TL = tile_heights.TL
		tile_verts.TR = tile_verts.TR + Vector3(0,tile_heights.TR,0)
		vertex_heights[coords].TR = tile_heights.TR
		tile_verts.BL = tile_verts.BL + Vector3(0,tile_heights.BL,0)
		vertex_heights[coords].BL = tile_heights.BL
		tile_verts.BR = tile_verts.BR + Vector3(0,tile_heights.BR,0)
		vertex_heights[coords].BR = tile_heights.BR
		
		var side1
		var side2
		
		side2 = tile_verts.BL - tile_verts.TL
		side1 = tile_verts.TR - tile_verts.TL
		tile_normals.TL = side1.cross(side2).normalized()
		
		side2 = tile_verts.TL - tile_verts.TR
		side1 = tile_verts.BR - tile_verts.TR
		tile_normals.TR = side1.cross(side2).normalized()
		
		side2 = tile_verts.BR - tile_verts.BL
		side1 = tile_verts.TL - tile_verts.BL
		tile_normals.BL = side1.cross(side2).normalized()
		
		side2 = tile_verts.TR - tile_verts.BR
		side1 = tile_verts.BL - tile_verts.BR
		tile_normals.BR = side1.cross(side2).normalized()	

	#######################################
		tile_data.materials = data.materials
		tile_data.height = data.height
		tile_data.pieces = data.get("pieces", null)
		tile_data.tile_normals = tile_normals
		tile_data.tile_verts = tile_verts
		tile_data.chunk = map_ref.get_chunk_number(coords)
		tiles[coords] = tile_data

	for coords in tiles:
		var tile_data = tiles[coords]
		if tile_data.get("pieces") != null:
			for piece in tile_data.pieces:
				register_entity(coords, piece)


func update_tile_data(coords, tile_data):
	var old_data = tiles[coords]
	
	for entity in old_data.entities:
		entity.remove()
	
	
	var mats = tile_data.get("materials")
	var height = tile_data.get("height")
	var pieces = tile_data.get("pieces")
	
	if mats != null:
		tiles[coords].materials = mats
	if height != null:
		tiles[coords].height = height
	if pieces != null:
		tiles[coords].pieces = pieces
		
	if tile_data.get("pieces") != null:
		for piece in tile_data.pieces:
			register_entity(coords, piece)		

## Faces are constructed TL BL BR - TL BR TR
func get_location_height(location):
	var coord = map_ref.world_to_grid(location)
	var tile = tiles.get(coord, null)
	if(tile == null):
		db.dbug("Could not find location: " + str(location) + " on the tile map. Calculated coordinate is: " + str(coord), "Chunk " + name)
	if(tile != null):
		location.y = max(max( tile.tile_verts.TL.y, tile.tile_verts.TR.y), max(tile.tile_verts.BL.y, tile.tile_verts.BR.y)) + 0.2
		var tri1 = Geometry.ray_intersects_triangle(location, Vector3(0,-1,0), tile.tile_verts.TL, tile.tile_verts.BL, tile.tile_verts.BR)
		if(tri1 != null):
			return tri1.y
		tri1 = Geometry.ray_intersects_triangle(location, Vector3(0,-1,0), tile.tile_verts.TL, tile.tile_verts.BR, tile.tile_verts.TR)
		if(tri1 != null):
			return tri1.y
		
func draw():
	verts = PoolVector3Array()
	uvs = PoolVector2Array()
	normals = PoolVector3Array()
	indices = PoolIntArray()
	
	var arr = []
	arr.resize(Mesh.ARRAY_MAX)
	for tile in tiles:
		var tile_data = tiles[tile]	
		if(tile_data.get("corners") != null):
			for vert in tile_data.corners:
				tile_data[vert] = verts.size()
				verts.append(tile_data.tile_verts[vert])
				normals.append(tile_data.tile_normals[vert])
				var floors = db.stores.tiles.get_floor(tile_data.materials.floors, vert)
				uvs.append(floors)
	
			indices.append(tile_data.TL)
			indices.append(tile_data.BL)
			indices.append(tile_data.BR)
			
			indices.append(tile_data.TL)
			indices.append(tile_data.BR)
			indices.append(tile_data.TR)
		else:
			print("found empty corners")
	
	# Create mesh surface from mesh array.
	
	arr[Mesh.ARRAY_VERTEX] = verts
	arr[Mesh.ARRAY_TEX_UV] = uvs
	arr[Mesh.ARRAY_NORMAL] = normals
	arr[Mesh.ARRAY_INDEX] = indices
	
	floors.mesh = ArrayMesh.new()
	if arr[Mesh.ARRAY_VERTEX].size() == 0:
		return
	floors.mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arr) # No blendshapes or compression used.	
	floor_collider.shape = floors.mesh.create_trimesh_shape()

func calculate_y(coord):
	var result = {
		"TL":0,
		"TR":0,
		"BL":0,
		"BR":0
	}
	
	var heights = []
	
	var avg1
	var avg2
	var avg3
	var avg4
	
	var total
	
	# TL
	avg1 = map_ref.height_map.get(coord + Vector2(-1, 0))
	avg2 = map_ref.height_map.get(coord)
	avg3 = map_ref.height_map.get(coord + Vector2(-1, 1))
	avg4 = map_ref.height_map.get(coord + Vector2(0,1))
	if avg1 != null:
		heights.push_back(avg1)
	if avg2 != null:
		heights.push_back(avg2)
	if avg3 != null:
		heights.push_back(avg3)
	if avg4 != null:
		heights.push_back(avg4)
	
	total = 0
	for height in heights:
		total = total + height
	result.TL = total / heights.size()
	heights.clear()
	
	#TR
	avg1 = map_ref.height_map.get(coord + Vector2(1,0))
	avg2 = map_ref.height_map.get(coord + Vector2(1,1))
	avg3 = map_ref.height_map.get(coord + Vector2(0,1))
	avg4 = map_ref.height_map.get(coord)
	if avg1 != null:
		heights.push_back(avg1)
	if avg2 != null:
		heights.push_back(avg2)
	if avg3 != null:
		heights.push_back(avg3)
	if avg4 != null:
		heights.push_back(avg4)

	total = 0
	for height in heights:
		total = total + height
	result.TR = total / heights.size()
	heights.clear()
	#BL
	avg1 = map_ref.height_map.get(coord + Vector2(-1,0))
	avg2 = map_ref.height_map.get(coord + Vector2(-1,-1))
	avg3 = map_ref.height_map.get(coord + Vector2(0,-1))
	avg4 = map_ref.height_map.get(coord)
	if avg1 != null:
		heights.push_back(avg1)
	if avg2 != null:
		heights.push_back(avg2)
	if avg3 != null:
		heights.push_back(avg3)
	if avg4 != null:
		heights.push_back(avg4)

	total = 0
	for height in heights:
		total = total + height
	result.BL = total / heights.size()
	heights.clear()
	
	#BR
	avg1 = map_ref.height_map.get(coord + Vector2(1,0))
	avg2 = map_ref.height_map.get(coord + Vector2(1,-1))
	avg3 = map_ref.height_map.get(coord + Vector2(0,-1))
	avg4 = map_ref.height_map.get(coord)	
	if avg1 != null:
		heights.push_back(avg1)
	if avg2 != null:
		heights.push_back(avg2)
	if avg3 != null:
		heights.push_back(avg3)
	if avg4 != null:
		heights.push_back(avg4)

	total = 0
	for height in heights:
		total = total + height
	result.BR = total / heights.size()
	heights.clear()	
	
	
	return result
