# Map Data




```JSON
{
    "header":{
        "name":map_name,
        "origin":{
            "local":originLocal,
            "global":originGlobal,
        "seed":string
        },
        "resolution":{
            "x":x-size in tiles,
            "y":y-size in tiles,
        }
    },
    "tiles":{
        n:{
            "height":height,
            "materials":{
                "floors":floor_type,
            },
            "pieces":{
                
            }
        }
    }

    ]
}
```
