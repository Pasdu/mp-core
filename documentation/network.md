Network Base Class


## Properties

players_by_id = {}
reference player object by their network identity

players_connected = []
simple array of all player objects

players_by_nickname = {}
reference player object by their chosen nickname

admins_connected = {}
administrators by ID connected to the server

## Signals

add_client(id)

output(level, text)
Use this to capture debugging and event messages that pertain to the network. IE Console output.
