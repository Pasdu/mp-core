# Database Configuration

MP-Core relies on a singleton called DB for the majority of its functions. This singleton is designed to be a plugin/module loader. The modules it loads are called `stores`. There are a variety of premade stores that can be used to extend the engine or are required by the engine for certain features to function, but stores are easy to write as well.

The core uses a configuration JSON object to determine which stores to use and what data they initialize with and contain.

# How to Configure the Database

The simplest way to make a database configuration is to create a file named `configuration.md` in the root of your project `res://configuration.md`.

Within this configuration file create a method called database_configuration(). This allows you to freely configure the DB singleton you created. From here, how you want to proceed is up to you, but take a look at an example of a configuration loader completed to see how I do it.

```
extends Node


static func database_configuration():
	var stores_config = {
		"mp":{
			"data":{
				"my_player":{
					"path":"res://player/local.tscn",
					"data":{}
				},
				"other_player":{
					"path":"res://player/remote.tscn",
					"data":{}
				}
			}
		},
		"profiles":{
			"store":"res://mp-core/db/profiles.gd",
			"data":{
				"profile-template":"res://db/profile.gd/"
			}
			
		},
		"world":{
			"data":{
				"chunk":{
					"path":"res://map/chunk.tscn",
					"data":{}
				},
				"server_map":{
					"path":"res://map/map-client.tscn",
					"data":{}
				},
				"client_map":{
					"path":"res://map/map-server.tscn",
					"data":{}
				}
			},
		},
		"tiles":{
			"store":"res://mp-core/db/block_references.gd",
			"data":{
				"sets":{
					"grounds":{
						#@imgpath is the resource path to the image file of the atlas
						#that these tiles are located on.
						"meta":{
							"img_path":"res://img/ground_tiles.png",
							"tile_size":32
						},
						"tiles":{
							"grass":{},
							"water":{},
							"forest":{},
							"dirt":{},
							"path":{},
						}
					}
				}
			}
		},

	for store in stores_config:
		var store_information = stores_config[store]
		print(store_information.get("data"))
		db.register_store(store, store_information.get("store",null), store_information.get("data", null) )

	db.initialize()

```

The database system in mp-core uses a system called `stores` as modules that can be loaded into the database. This allows you to create a class that is responsible for handling the way that your internal databases are managed. Stores are very powerful because they can be filled with premade modules and then replaced with bespoke ones over time and thus have great 'quickstart' applications. For example, for multiplayer games the mp store allows you to quickly get your own local and remote player scene set up and working with the network.

The `database_configuration` method above is a big chunk of json configuration (the config itself), with a simple loop at the end that registers each of the stores configured in the JSON object itself.

## Premade Store Configs

Here are some examples of built-in Store configurations you might need for your project.

### MP Store

Uses a default store to create references to the multiplayer objects used by the engine. When the built in map script spawns a player it uses these locations.

```JSON
		"mp":{
			"data":{
				"my_player":{
					"path":"res://player/local.tscn",
					"data":{}
				},
				"other_player":{
					"path":"res://player/remote.tscn",
					"data":{}
				}
			}
		},
```

### Profiles Module

The profile module is a custom module created to allow users to switch between profiles in the game. This can be used to keep separate characters and settings for multiple users on the computer.

**Properties**:

`profile-template` points to the player profile script. This is what will be instantiated when you create a new profile.

```JSON
		"profiles":{
			"store":"res://mp-core/db/profiles.gd",
			"data":{
				"profile-template":"res://db/profile.gd/"
			}
			
		},
```

### World Module

The world module allows you to use mp-core's world generation features with custom map and chunk scene files.

```JSON
		"world":{
			"data":{
				"chunk":{
					"path":"res://map/chunk.tscn",
					"data":{}
				},
				"server_map":{
					"path":"res://map/map-client.tscn",
					"data":{}
				},
				"client_map":{
					"path":"res://map/map-server.tscn",
					"data":{}
				}
			},
		},
```

### Tiles Module

```JSON
		"tiles":{
			"store":"res://mp-core/db/block_references.gd",
			"data":{
				"sets":{
					"grounds":{
						#@imgpath is the resource path to the image file of the atlas
						#that these tiles are located on.
						"meta":{
							"img_path":"res://img/ground_tiles.png",
							"tile_size":32
						},
						"tiles":{
							"grass":{},
							"water":{},
							"forest":{},
							"dirt":{},
							"path":{},
						}
					}
				}
			}
		},
```