extends Node

# The currently loaded profile data
var loaded_data = {}

# Available profiles for loading
var data_manifest = {}

var profile_template = null
var manifest_template = null

func init(store, data):
	pass


func load_profile(profile_path = "profile"):
	var save_game = File.new()
	if not save_game.file_exists("user://" + profile_path + ".save"):
		return # Error! We don't have a save to load.

	save_game.open("user://profile.save", File.READ)
	# Get the saved dictionary from the next line in the save file
	var profile_data = parse_json(save_game.get_line())
	
	handle_profile_data(profile_data)
	
	save_game.close()

func create_profile(label):
	var profile = profile_template.duplicate()
	data_manifest[label] = profile

func handle_profile_data(data):
	pass
