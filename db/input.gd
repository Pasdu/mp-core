extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func init(store, data):
	for mapping in data:
		var mapping_data = data[mapping]
		var event = data_to_event(mapping_data)
		
		if event is String:
			db.dbug("Problem registering input event \"" + mapping + "\" received error: " + event, "Input Store")
		else:
			register_input_action(mapping)
			InputMap.action_add_event(mapping, event)
		
		
func register_input_action(action):
	if(InputMap.has_action(action)):
		db.dbug("Attempted to register duplicate action: " + action, "Input")
	InputMap.add_action(action)

func data_to_event(data):
	var event
	var event_type = data.get("event_type")
	if event_type == null:
		return "event_type is null"
	match(data.event_type):
		"key":
			event = InputEventKey.new()
			event.scancode = data.scancode
			event.alt = data.get("alt", false)
			event.command = data.get("command", false)
			event.control = data.get("control", false)
			event.meta = data.get("meta", false)
			event.shift = data.get("shift", false)
		"mouse-btn":
			event = InputEventMouseButton.new()
			event.button_index = data.button_index
			event.doubleclick = data.doubleclick			
		"mouse-motion":
			event = InputEventMouseMotion.new()
		"joy-btn":
			event = InputEventJoypadButton.new()
			data.button_index = event.button_index			
		"joy-motion":
			event = InputEventJoypadMotion.new()
			event.axis = data.axis			
			event.axis_value = data.axis_value
		
	return event
func event_to_data(event):
	var data = {
		"event_type":""
	}
	
	if event is InputEventKey:
		data.event_type = "key"
		data.scancode = event.scancode
		data.alt = event.alt
		data.command = event.command
		data.control = event.control
		data.meta = event.meta
		data.shift = event.shift
		
	elif event is InputEventMouseButton:
		data.event_type = "mouse-btn"
		
		data.button_index = event.button_index
		data.doubleclick = event.doubleclick
		
	elif event is InputEventMouseMotion:
		data.event_type = "mouse-motion"
	elif event is InputEventJoypadButton:
		data.event_type = "joy-btn"
		
		data.button_index = event.button_index
		
	elif event is InputEventJoypadMotion:
		data.event_type = "joy-motion"
		data.axis = event.axis
		data.axis_value = event.axis_value
