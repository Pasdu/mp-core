var store_name
var store_data = {}

func register_item(label, path, data):
	var object = load(path)
	if object == null:
		db.dbug("ERROR: Couldn't load object for store " + store_name + " on item " + label, "STORE")
		return
	store_data[label] = {"object":load(path), "data":data}

func get_item(query):
	return store_data.get(query, null)

func init(store, data):
	store_name = store
	if data != null:
		for item in data:
			register_item(item, data[item].path, data[item].get("data", null))
	
	
