extends Node

var current_message = null

var messages = []

var items
var timer = 0

var tooltip_offset = Vector2(0, -20)

func add_tooltip(message, delay):
	messages.push_back( { "msg":message, "delay":delay } )
	
	if current_message == null:
		play_next_message()
	
func clear_tooltip():
	self.text = ""
	self.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if messages.size() > 0:
		if messages[0].delay != -1 and timer + delta > messages[0].delay:
			timer = 0
			play_next_message()

func play_next_message():
	self.text = messages[0].msg
	self.rect_size.x = 0
	self.visible = true
	messages.pop_front()

func _input(event):
	if self.visible == true and event is InputEventMouseMotion:
		self.rect_global_position = event.position + event.relative + tooltip_offset
