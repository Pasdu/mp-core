extends Node

signal init_complete

var init = false

var stores = {
	
}

var tooltip

var dbug_node
func dbug(message, title):
	if(dbug_node == null):
		print("something is pre-empting dbug method in core_db" + title + " " + message)
	else:
		dbug_node.debug_message(message,title)
		
func _ready():
	var dbug_object = load("res://mp-core/dbug/dbug.tscn").instance()
	dbug_object.init()
	dbug_node = dbug_object
	tooltip = get_tree().get_root().get_node_or_null("Game/utooltip")
	if tooltip != null:
		tooltip.visible = false
		tooltip("hi")
	get_tree().get_root().call_deferred("add_child", dbug_object)
	initialize()

func tooltip(message, delay = -1):
	tooltip.add_tooltip(message, delay)

func register_store( identifier, path, data ):
	stores[identifier] = { "store":path, "data":data }
	dbug("Store registered " + identifier, "DB")

var focal_point = null


func get_world_object(query):
	var found_store = stores.get(query.store)
	
	if found_store != null:
		return found_store.get_item(query.query)
	else:
		dbug("couldn't find any object", "get_world_object")

func initialize(configuration_path = null):
	if configuration_path == null:
		configuration_path = "res://configuration.gd"
	dbug("Initializing from path " + configuration_path, "Core_DB initialize()")
	var config = load(configuration_path)
	config.database_configuration()	
	
	for store in stores:
		var store_data = stores[store]
		if store_data.store == null:
			store_data.store = "res://mp-core/db/store.gd"
		var store_instance = load(store_data.store).new()
		store_instance.init(store, store_data.data)
		stores[store] = store_instance
	
	init = true
	emit_signal("init_complete")

func get_my_profile():
	pass


