extends Node

var data_manifest

func _init():
	var manifest = File.new()
	if not manifest.file_exists("user://manifest.save"):
		db.dbug("No Manifest file found, creating anew", "Profiles")
		return # Error! We don't have a save to load.
	db.dbug("Found Manifest, reading profiles", "Profiles")
	manifest.open("user://manifest.save", File.READ)
	# Get the saved dictionary from the next line in the save file
	data_manifest = parse_json(manifest.get_line()) 
	#db.dbug(data_manifest, "profiles")
	

func write_manifest():
	var manifest = File.new()
	manifest.open("user://manifest.save", File.WRITE)
	manifest.store_line(data_manifest)
