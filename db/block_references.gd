extends "res://mp-core/db/store.gd"

const DEFAULT_MATERIALS = {
		"floors":{"width":24.0 , "height":32.0 , "x_tiles":3.0 , "y_tiles":4.0 },
	}


var store = {"sets":{}}

# The data you provide to init should look like this:
var sample_data = {
	"sets":{
		"grounds":{
			#@imgpath is the resource path to the image file of the atlas
			#that these tiles are located on.
			"meta":{
				"img_path":"res://img/grounds.png",
				"tile_size":8
			},
			"tiles":{
				"gravel":{},
				"grass":{},
				"forest":{},
				"magma":{},
				"volcanic_rock":{},
				"dirt":{},
				"water":{},
				"polluted_water":{},
				"road":{},
				"road_lined_corner":{},
				"road_lined":{},
				"cobblestone":{},
				"cobblestone_path":{}
			}
		}
	}
}

func get_floor(floor_name, vert):
	if store.sets.grounds.tiles.get(floor_name, null) == null:
		db.dbug("Couldn't find tile: " + floor_name, "block_references")
	return store.sets.grounds.tiles.get(floor_name, null).get(vert)

# Called when the node enters the scene tree for the first time.
func init(store_name, init_data):
	var materials = init_data

	var u = 0.0
	var v = 0.0
	for tile_set in init_data.sets:
		var mat = materials.sets[tile_set]
		var metadata = mat.meta
		
		var tile_image = load(metadata.img_path)
		var image_width = tile_image.get_width()
		var image_height = tile_image.get_height()
		
		var x_tiles = image_width / metadata.tile_size
		#var y_tiles = image_height / metadata.tile_size
		
		var pixel_size_u = 1.0/image_width
		var pixel_size_v = 1.0/image_height
		
		var box_size_u = pixel_size_u * metadata.tile_size
		var box_size_v = pixel_size_v * metadata.tile_size
		
		var spacing_u = pixel_size_u / 3.0
		var spacing_v = pixel_size_v / 3.0
		
		var i = 0
		store.sets[tile_set] = { "tiles":{} }
		for tile in init_data.sets[tile_set].tiles:
			var x = i % int(x_tiles)
			var y = floor(i / x_tiles)
			
			u = x * box_size_u
			v = y * pixel_size_v * metadata.tile_size
			
			store.sets[tile_set].tiles[tile] = {}
			
			store.sets[tile_set].tiles[tile].TL = Vector2(u + spacing_u, v + spacing_v)
			store.sets[tile_set].tiles[tile].TR = Vector2(u+ box_size_u - spacing_u, v + spacing_v)
			store.sets[tile_set].tiles[tile].BL = Vector2(u + spacing_u, v + box_size_v)
			store.sets[tile_set].tiles[tile].BR = Vector2(u + box_size_u - spacing_u, v + box_size_v - spacing_v)
			i = i + 1
			
	sample_data = null
