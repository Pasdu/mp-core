extends "res://mp-core/network/dynamic_network_entity.gd"

signal direction_set(new_direction)
var last_direction


var sprite

puppet func update_orientation(orientation):
	rotation = orientation

func _process(_delta):
	if(db.focal_point != null):
		
		var focal_vector = db.focal_point.transform.basis.z
		var object_vector = transform.basis.z
		
		var focal_angle = Vector2(focal_vector.x, focal_vector.z).angle()			
		var object_angle = Vector2(object_vector.x, object_vector.z).angle()
		
		var focal_prime = focal_angle + PI
		var object_prime = object_angle + PI
		
		var angledelta = focal_prime - object_prime
		
		if angledelta > -PI/4 and angledelta < PI/4:
			set_direction("back")	
		elif(angledelta >= PI/4 and angledelta < (3*PI)/4):
			set_direction("right")
		elif angledelta <= -PI/4 and angledelta > -(3*PI/4):
			set_direction("left")
		else:
			set_direction("front")
	else:
		pass
	
func set_direction(direction):
	if(direction != last_direction):
		emit_signal("direction_set", direction)
		last_direction = direction
		
