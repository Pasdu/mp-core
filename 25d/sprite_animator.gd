extends Sprite3D

var current_animation = {
	"animation":null,
	"delta":0.0,
	"frame":0
}

const ANI_STATES = {
	"started":0,
	"finished":1
}

var triggers = {"animation":{}, "frame":{}}

func register_frame_trigger(frame, method):
	if triggers.frame.get(frame) == null:
		triggers.frame[frame] = []
	triggers.frame[frame].push_back(method)
	
func fire_frame_triggers(frame):
	if(triggers.frame.get(frame) != null):
		for trigger in triggers.frame[frame]:
			trigger.call_func()
			
func register_animation_trigger(animation, state, method):
	if triggers.animation.get(animation) == null:
		triggers.animation[animation] = {}
		triggers.animation[animation][state] = []
	triggers.animation[animation][state].push_back(method)

func fire_animation_triggers(animation, state):
	if(triggers.animation.get(animation) != null):
		if(triggers.animation[animation].get(state) != null):
			for trigger in triggers.animation[animation][state]:
				trigger.call_func()

func play_animation(data, frame = 0):
	current_animation.animation = data
	current_animation.delta = 0.0
	current_animation.frame = frame - 1
	fire_animation_triggers(data.name, 0)
	get_next_frame()

func switch_animation(data):
	current_animation.animation = data
	get_next_frame()

func get_next_frame():
	var animation = current_animation.animation
	if(animation.loop):
		current_animation.frame = (current_animation.frame + 1) % (animation.frames.size())
		frame = animation.frames[current_animation.frame]
		fire_frame_triggers(animation.frames[current_animation.frame])			
	else:
		var frameVal = current_animation.frame + 1
		current_animation.frame = min(frameVal, animation.frames.size()-1)
		frame = animation.frames[current_animation.frame]
		fire_frame_triggers(animation.frames[current_animation.frame])
		if(frameVal == animation.frames.size()):
			current_animation.animation = null
			fire_animation_triggers(animation.name, 1)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if current_animation.animation != null:
		current_animation.delta += delta
		if(current_animation.delta > 1.0 / current_animation.animation.frame_rate):
			current_animation.delta = 0.0
			get_next_frame()
